#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>//Biblioteca do tempo.

struct{//Estrutura que armazena os dados que seram colocados no arquivo
       char jog1[20], jog2[20], vencedor[20];//Nomes dos jogadores e vencedor
       int cont1, cont2, pont_max;//Contador das peças.
       }dama;

  FILE *historico;//Inicia arquivo que armazena nome dos jogadores. 

  void jogador1(char matriz[8][8],char jogador, int oposto)//Funções tabuleiro e jogadores.
  {
  system("color 20");
  int i,j,l,c,li,co,opcao=0;
  
  do//Laço jogo da dama
  {
    system("cls");
    if(jogador==1)//Definição jogador peça branca
       printf("\n\n----------------    A vez e a do jogador da peca Preta ----------\n\n\n\n");
    if(jogador==2)//Definição jogador peça preta
       printf("\n\n----------------    A vez e a do jogador da peca Branca  ------------\n\n\n\n");
       printf("\t \t\t   0 1 2 3 4 5 6 7 \n");//Coordenada do tabuleiro.
    for(i=0;i<8;i++)//Desenho do tabuleiro com matriz e indices.
   {                       
       printf("\n\t\t\t%d  ",i);
    for(j=0;j<8;j++)
       printf("%c ",matriz[i][j]);
   }
  
  printf("\n\n\t******  Cordenada da peca  *********");//Escolhe a peça que vai mexer.
  printf("\n\tLinha: ");
  scanf("%d",&l);
  printf("\n\tColuna: ");
  scanf("%d",&c);
  
  printf("\n\n\t***Cordenada da posicao que vai ocupar*");//Posição que a peça vai ocupar.
  printf("\n\tLinha: ");
  scanf("%d",&li);
  printf("\n\tColuna: ");
  scanf("%d",&co);
  if((li+co)%2==0)//Condição para só andar quando a soma dos indices forem par.
  {
     if((jogador==1&&l<li)||(jogador==2&&l>li))//Funções para a peça só andar uma casa.
     {
        printf("linha\n");
      //Condições para comer peças.
        if(c-1==co||c+1==co)
        {
                        if(co==c-1)
                        {
                        matriz[li][co] = jogador;
                        matriz[l][c] = 0;
                        opcao++;
                        }
                        if(co==c+1)
                        {
                        matriz[li][co] = jogador;
                        matriz[l][c] = 0;
                        opcao++;
                        }
        }

        if(matriz[l+1][c+1]==oposto)
        {
                        if(c+2==co)
                        {
                                 matriz[li][co] = jogador;
                                 matriz[l][c] = 0;
                                 opcao++;
                                 matriz[l+1][c+1]=0;
                        }
        }
        
        if(matriz[l+1][c-1]==oposto)
        {
                        if(c-2==co)
                        {
                                 matriz[li][co] = jogador;
                                 matriz[l][c] = 0;
                                 matriz[l+1][c-1]=0;
                                 opcao++;
                        }
        }
  
        if(matriz[l-1][c+1]==oposto)
        {
                        if(c+2==co)
                        {
                                 matriz[li][co] = jogador;
                                 matriz[l][c] = 0;
                                 opcao++;
                                 matriz[l-1][c+1]=0;
                        }
        }
        
        if(matriz[l-1][c-1]==oposto)
        {
                        if(c-2==co)
                        {
                                matriz[li][co] = jogador;
                                matriz[l][c] = 0;
                                matriz[l-1][c-1]=0;
                                opcao++;
                        }
        }
  }
  else printf("\n\n\t\t_____Movimento Invalido!!___\n\t\t___Jogue Novamente___\n");//Para o caso da posição invalida.
  }
  system("pause");
  }while(opcao!=1);//Termina o laco do jogo da dama.
}
void vencedor(char matriz[8][8])//Função que define quem ganhou
{
       int i,j;
         for(i=0;i<8;i++)
           for(j=0;j<8;j++)
           {
             if(matriz[i][j]==2)
             dama.cont1++;//Quantidade de peças pretas jogador 2.
             if(matriz[i][j]==1)
             dama.cont2++;//Quantidade de peças brancas jogaddor 1.
           }
             if(dama.cont1>dama.cont2)//Contagem de quem tem mais peças, para saber quem ganhou.
             {
               printf("\n\tO vencedor eh o jogador...-> %s Total de pecas: %d\n\n",dama.jog1,dama.cont1);//Fala quem ganhou(preta), e com quantas peças.
               strcpy(dama.vencedor, dama.jog1);//Pontuação do contador da peça preta
               dama.pont_max=dama.cont1;//Pontuação maxima do cont. peça preta
              } 
             else
             {
             if(dama.cont1<dama.cont2)//Analisa quem tem mais peças
             {  
               printf("\n\tO vencedor é o jogador...-> %s Total de pecas: %d\n\n",dama.jog2,dama.cont2);//Fala quem ganhou(branca), e com quantas peças.
               strcpy(dama.vencedor, dama.jog2);
               dama.pont_max=dama.cont2;//Pontuação maxima peça branca
             }
             else{
               printf("\n\tEmpate!\n\t%s: %d\n\t%s: %d\n\n\n",dama.jog1,dama.cont1,dama.jog2,dama.cont2);//Se empate
               strcpy(dama.vencedor,"jogo empatado");
               dama.pont_max=dama.cont2;//Pontuação maxima.
                }
             }
 }
   void registro(void){//Função registro do gamer no arquivo
       fwrite(&dama, sizeof(dama), 1, historico);
                       }
   void escreve(void){//Função que escreve no arquivo.
       rewind(historico);
       while(fread(&dama, sizeof(dama), 1, historico)==1)//Ler todas as informações.
       printf("\nJogador peca preta: %s\nJogador peca branca: %s\nVencedor: %s        Total de pecas: %d\n\n",dama.jog1, dama.jog2, dama.vencedor, dama.pont_max);
                     }
     
int main(int argc, char *argv[])
{
    //Declaração de todo o tabuleiro.
   char matriz[8][8];
   int i,j, jogador=2,oposto=1,opcao=0, opcao2;
     dama.cont1=dama.cont2=0;
     historico=fopen("Historico_dama.txt", "a+");
   
  matriz[0][0]= 1 ;    matriz[0][1]= 178;   matriz[0][2]= 1;   matriz[0][3] = 178; matriz[0][4] = 1;  matriz[0][5]= 178; matriz[0][6]= 1;   matriz[0][7]= 178; 
  matriz[1][0]= 178;   matriz[1][1]= 1;     matriz[1][2]= 178; matriz[1][3] = 1;   matriz[1][4] = 178;matriz[1][5]= 1;   matriz[1][6]= 178; matriz[1][7]= 1; 
  matriz[2][0]= 1;     matriz[2][1]= 178;   matriz[2][2]= 1;   matriz[2][3] = 178; matriz[2][4] = 1;  matriz[2][5]= 178; matriz[2][6]= 1;   matriz[2][7]= 178 ; 
  matriz[3][0]= 178;   matriz[3][1]= 0;     matriz[3][2]= 178; matriz[3][3] = 0;   matriz[3][4] = 178;matriz[3][5]= 0;   matriz[3][6]= 178; matriz[3][7]= 0;  
  matriz[4][0]= 0;     matriz[4][1]= 178;   matriz[4][2]= 0;   matriz[4][3] = 178; matriz[4][4] = 0;  matriz[4][5]= 178; matriz[4][6]= 0;   matriz[4][7]= 178;
  matriz[5][0]= 178;   matriz[5][1]= 2;     matriz[5][2]= 178; matriz[5][3] = 2;   matriz[5][4] = 178;matriz[5][5]= 2;   matriz[5][6]= 178; matriz[5][7]= 2;
  matriz[6][0]= 2;     matriz[6][1]= 178;   matriz[6][2]= 2;   matriz[6][3] = 178; matriz[6][4] = 2;  matriz[6][5]= 178; matriz[6][6]= 2;   matriz[6][7]= 178;
  matriz[7][0]= 178;   matriz[7][1]= 2;     matriz[7][2]= 178; matriz[7][3] = 2;   matriz[7][4] = 178;matriz[7][5]= 2;   matriz[7][6]= 178; matriz[7][7]= 2; 
  do{
    system("color 47");
    //LOBBY
    printf("\n\t__________________________________\n");
    printf("\t__________________________________\n");
    printf("\t__________________________________\n");
    printf("\t_______                    _______\n");
    printf("\t_______                    _______\n");
    printf("\t_______   JOGO  DA  DAMA   _______\n");
    printf("\t_______                    _______\n");
    printf("\t__________________________________\n");
    printf("\t__________________________________\n");
    printf("\t__________________________________\n");
    printf("\tEscolha uma das opcoes abaixo:\n\n",135,228);
    printf("\t%c 1 Jogar.\n\t%c 2 Ajuda.\n\t%c 3 Historico.\n\t%c 4 Sair.\n\n",175,175,175,175,175);
    printf("\tOp%c%co: ",135,198);// Os números e colocar acentos.
    scanf("%d",&opcao2);//Escolher as opções do menu
    system("cls");
    switch(opcao2){
                case 1:
                     system("color 10");
                     printf("\a\n\n\t\t\tENTER PARA CONTINUAR.....");
                     printf("\n\n\n");
                     system("pause");
                     system("cls");
                     system("color 50");
                     printf("\n\n\t\tInforme nome do jogador peca Preta:\n\n\t\t\t\t");//Nome jogador peça preta.
                     scanf("%s",dama.jog1);
                     printf("\n\n\t\tInforme o nome do jogador peca Branca:\n\n\t\t\t\t ");//Nome jogador peça branca.
                     scanf("%s",dama.jog2);
                     
                     
                     //Conta o tempo para fim do jogo.
                     int tempo = clock();
                      while(clock()-tempo<120000)
                      {
                                               
                      jogador=1;oposto=2;
                      jogador1(matriz,jogador,oposto);
                      jogador=2;oposto=1;
                      jogador1(matriz,jogador,oposto);
                      }
                     system("cls");
                     system("color 20");
                     printf("\n\t\t..........PASSARAM 2 MINUTOS FIM DO JOGO...........\n\n\n\a");//Aparece na tela qunado se passa 2 minutos
                         vencedor(matriz);//Chama a função vencedor
                         registro();//Chama a função registro, que salva as informações.
                     system("pause");
                     system("cls");
                     break;
                case 2://Mostra as informações do jogo.
                     system("color 90");
                     printf("\a\n__________O QUE E O JOGO?__________");
                     printf("\n\t     O jogo de Damas e constituido por um Tabuleiro quadratico,\n\tdividido em 64 quadrados,com 24 Pecas,sendo 12 de cor branca\n\te 12 de cor preta. Exitem  8 linhas que estao na posicao  vertical,\n\te com 8 culunas na posicao horizantal.\n");
                     printf("\n___________  O OBJETIVO  ___________");
                     printf("\n\n\t      Comer o maior Numero de Pecas Possiveis.Quem \n\tDurante os 3 Minutos tiver  mais Pecas e o Vencedor!\n\n");
                     
                     printf("\n___________REGRAS O JOGO____________");
                     printf("\n\n\t1-Nao e permitido comer para tras.\n\t2-Pode comer uma peca,nao duas de uma vez.\n\t3-So anda uma casa por vez.\n\t4- O Jogo dura  3 Minutos.\n\t5-Nao e permitido Jogar com uma Peca do adversario.\n");
                     printf("__________________________\n\n");
                     system("pause");
                     system("cls");
                     break;
                case 3:
                     system("color 80");
                     system("pause");
                     system("cls");
                        escreve();//Chamada da função escreve.
                     system("pause");
                     system("cls");
                     break;
                case 4://Para sair do jogo.
                     system("color 70");
                     printf("\a\n\n\n\n\t\t\t  Fim do Jogo\n\n\n\n\n");
                     break;
    }
  }while(opcao2!=4);//Laço para terminar switch
      fclose(historico);//Fecha o arquivo.
      system("PAUSE");	
  return 0;
}
